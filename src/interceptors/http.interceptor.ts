import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';

import { errorMessage } from '../utils/errors';
import { SharedService } from '../services/shared';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {

  private token: string | null;
  private token$: Observable<string>;

  constructor(
    private store: Store<any>,
    private injector: Injector,
    private sharedService: SharedService,
    private translate: TranslateService,
  ) {
    this.token$ = this.store.select(_store => _store.auth ? _store.auth.access_token : '');
    this.token$.subscribe((token: string) => this.token = token);
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = request.headers.get('Authorization');
    let params = new HttpParams({fromString: request.params.toString()});
    if (params.has('type')) { params = params.delete('type'); }

    const _request = request.clone({
      params,
      headers: new HttpHeaders({
        Authorization: token  || `Bearer ${this.token}`,
      }),
    });

    return next.handle(_request).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 0) {
          this.sharedService.warning('Warning', 'Problemas de conexion con el servidor');
          return throwError(err);
        }
        const error = errorMessage(err);
        this.sharedService.error(this.translate.instant(error.message));
        return throwError(error);
      })
    );
  }
}
