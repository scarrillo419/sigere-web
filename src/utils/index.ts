import { objectToFormData } from 'object-to-formdata';

export const cleanFormRequest = (fields: any) => {
  delete fields.type;

  return fields;
};

export const prepareData = (fields: any) => {
  if ('type' in fields) {
    delete fields.type;
  }
  delete fields._id;

  return objectToFormData(fields, { indices: true });
};
