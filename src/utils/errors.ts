import { HttpErrorResponse } from '@angular/common/http';
import { isArray } from 'lodash';

export function errorMessage(error: HttpErrorResponse): any {
  if (error.error) {
    const err = error.error.error;
    const messages = [];

    if (isArray(err.message)) {
      for (const message of err.message) {
        if (message.constraints) {
          const constraints = Object.keys(message.constraints);
          for (const key of constraints) {
            messages.push(message.constraints[key]);
          }
        } else if (message.children && message.children.length > 0) {
          for (const messageChild of message.children) {
            if (messageChild.constraints && messageChild.constraints.length > 0) {
              const constraintsChild = Object.keys(messageChild.constraints);
              for (const key of constraintsChild) {
                messages.push(messageChild.constraints[key]);
              }
            } else if (messageChild.children && messageChild.children.length > 0) {
              for (const cc of messageChild.children) {
                if (cc.constraints) {
                  const constraintsChild = Object.keys(cc.constraints);
                  for (const key of constraintsChild) {
                    messages.push(cc.constraints[key]);
                  }
                }
              }
            }
          }
        }
      }
    } else {
      if (err.message && err.message.errmsg) {
        messages.push(err.message.errmsg);
      } else if (err.message) {
        messages.push(err.message);
      } else {
        messages.push(err);
      }

      return {
        message: messages.join(','),
        code: err.code,
      };
    }

    if (error.statusText) {
      return {
        message: error.statusText,
        code: error.status,
      };
    }

    return JSON.stringify(error);

  }

  return {
    message: 'Error inesperado intente de nuevo ',
    code: null,
  };
}
