import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

@Injectable({
  providedIn: 'root'
})

export class IsAuthenticatedGuard implements CanActivate {

  authenticated$: Observable<boolean>;

  constructor(
    public router: Router,
    private store: Store<any>
  ) {
    this.authenticated$ = this.store.select(_store => _store.auth.authenticated);
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.authenticated$.subscribe(data => {
      if (data) {
        this.router.navigate(['app']);
      }
    });
    return true;
  }

}
