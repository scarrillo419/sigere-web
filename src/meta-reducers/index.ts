import {ActionReducer, Action} from '@ngrx/store';
import {merge, pick} from 'lodash-es';

import { environment } from '../environments/environment';

function setSavedState(state: any) {
  localStorage.setItem(environment.localStorageKey, JSON.stringify(state));
}

function getSavedState(): any {
  return JSON.parse(localStorage.getItem(environment.localStorageKey));
}

// the keys from state which we'd like to save.
const stateKeys = ['auth'];

export function storageMetaReducer<S, A extends Action = Action>(reducer: ActionReducer<S, A>) {
  let onInit = true; // after load/refresh…
  return (state: S, action: A): S => {
    // reduce the nextState.
    const nextState = reducer(state, action);
    // init the application state.
    if (onInit) {
      onInit           = false;
      const savedState = getSavedState();
      return merge(nextState, savedState);
    }
    // save the next state to the application storage.
    const stateToSave = pick(nextState, stateKeys);
    setSavedState(stateToSave);
    return nextState;
  };
}
