import { createAction, props } from '@ngrx/store';

export enum AuthActionTypes {
  AuthSignin = '[Auth] Signin',
  AuthSignout = '[Auth] Signout',
  SetCurrentUser = '[Auth] Set Current User',
  VerifyUser = '[Auth] Verify User',
  Verified = '[Auth] Verified',
  RefreshToken = '[Auth] RefreshToken',
  Refreshed = '[Auth] Refreshed',
  AuthForgetPassword = '[Auth] Forget Password',
  AuthSuccessRequest = '[Auth] Success Request',
  AuthErrorRequest = '[Auth] Error Request',
}

export const signin = createAction(AuthActionTypes.AuthSignin, props<{ data }>());

export const signout = createAction(AuthActionTypes.AuthSignout);

export const setCurrentUser = createAction(AuthActionTypes.SetCurrentUser, props<{ data }>());

export const verifyUser = createAction(AuthActionTypes.VerifyUser, props<{ data }>());

export const verified = createAction(AuthActionTypes.Verified);

export const refreshToken = createAction(AuthActionTypes.RefreshToken, props<{ data }>());

export const refreshed = createAction(AuthActionTypes.Refreshed);

export const forgetPassword = createAction(AuthActionTypes.AuthForgetPassword, props<{ data }>());

export const successRequest = createAction(AuthActionTypes.AuthSuccessRequest);

// export const errorRequest = createAction(AuthActionTypes.AuthErrorRequest, props<{ data }>());
export const errorRequest = createAction(AuthActionTypes.AuthErrorRequest, props<{ data }>());

export const authActions = {
  signin,
  signout,
  setCurrentUser,
  verifyUser,
  verified,
  refreshToken,
  refreshed,
  forgetPassword,
  successRequest,
  errorRequest,
};
