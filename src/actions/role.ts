import { createAction, props } from '@ngrx/store';

export enum RoleActionTypes {
  GetAll = '[Role] Get All',
  Get = '[Role] Get',
  SetAll = '[Role] Set All',
  Set = '[Role] Set',
  New = '[Role] New',
  CancelNew = '[Role] Cancel New',
  Save = '[Role] Save',
  Edit = '[Role] Edit',
  Update = '[Role] Update',
  Delete = '[Role] Delete',
  SuccessRequest = '[Role] Success Request',
  ErrorRequest = '[Role] Error Request',
}

export const getAllRoles = createAction(RoleActionTypes.GetAll);

export const getRole = createAction(RoleActionTypes.Get, props<{ data }>());

export const setAllRoles = createAction(RoleActionTypes.SetAll, props<{ data }>());

export const setRole = createAction(RoleActionTypes.Set, props<{ data }>());

export const newRole = createAction(RoleActionTypes.New);

export const cancelNewRole = createAction(RoleActionTypes.CancelNew);

export const saveRole = createAction(RoleActionTypes.Save, props<{ data }>());

export const editRole = createAction(RoleActionTypes.Edit, props<{ data }>());

export const updateRole = createAction(RoleActionTypes.Update, props<{ id, data }>());

export const deleteRole = createAction(RoleActionTypes.Delete, props<{ id }>());

export const successRequest = createAction(RoleActionTypes.SuccessRequest);

export const errorRequest = createAction(RoleActionTypes.ErrorRequest);

export const roleActions = {
  getAllRoles,
  getRole,
  setAllRoles,
  setRole,
  newRole,
  cancelNewRole,
  saveRole,
  editRole,
  updateRole,
  deleteRole,
  successRequest,
  errorRequest,
};
