import { createAction, props } from '@ngrx/store';

export enum UserActionTypes {
  GetAll = '[User] Get All',
  Get = '[User] Get',
  SetAll = '[User] Set All',
  Set = '[User] Set',
  New = '[User] New',
  CancelNew = '[User] Cancel New',
  Save = '[User] Save',
  Edit = '[User] Edit',
  Update = '[User] Update',
  Delete = '[User] Delete',
  GetRoles = '[User] Get Roles',
  SetRoles = '[User] Set Roles',
  SuccessRequest = '[User] Success Request',
  ErrorRequest = '[User] Error Request',
}

export const getAllUsers = createAction(UserActionTypes.GetAll);

export const getUser = createAction(UserActionTypes.Get, props<{ data }>());

export const setAllUsers = createAction(UserActionTypes.SetAll, props<{ data }>());

export const setUser = createAction(UserActionTypes.Set, props<{ data }>());

export const newUser = createAction(UserActionTypes.New);

export const cancelNewUser = createAction(UserActionTypes.CancelNew);

export const saveUser = createAction(UserActionTypes.Save, props<{ data }>());

export const editUser = createAction(UserActionTypes.Edit, props<{ data }>());

export const updateUser = createAction(UserActionTypes.Update, props<{ id, data }>());

export const deleteUser = createAction(UserActionTypes.Delete, props<{ id }>());

export const getRoles = createAction(UserActionTypes.GetRoles);

export const setRoles = createAction(UserActionTypes.SetRoles, props<{ data }>());

export const successRequest = createAction(UserActionTypes.SuccessRequest);

export const errorRequest = createAction(UserActionTypes.ErrorRequest);

export const userActions = {
  getAllUsers,
  getUser,
  setAllUsers,
  setUser,
  newUser,
  cancelNewUser,
  saveUser,
  editUser,
  updateUser,
  deleteUser,
  getRoles,
  setRoles,
  successRequest,
  errorRequest,
};
