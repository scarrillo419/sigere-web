import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../src/environments/environment';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(public http: HttpClient) {}

  signin(params) {
    return this.http.post(`${apiUrl}/api/auth/signin`, params);
  }

  verifyUser(params: any) {
    const headers = new HttpHeaders({ Authorization: `Bearer ${params.data}` });
    return this.http.post(`${apiUrl}/api/auth/verify-user`, null, { headers });
  }

  refreshToken(params: any) {
    const headers = new HttpHeaders({ Authorization: `Bearer ${params.data}` });
    return this.http.post(`${apiUrl}/api/auth/refresh-token`, null, { headers });
  }
}
