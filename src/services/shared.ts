import { Injectable } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Injectable({ providedIn: 'root' })
export class SharedService {

  constructor(private notificationService: NzNotificationService) { }

  error(title: string, content?: string | null) {
    this.notificationService.error(
      title,
      content || null,
      { nzClass: `system-http_notification error ${!content && 'only-title'}` },
    );
  }

  warning(title: string, content?: string | null) {
    this.notificationService.warning(
      title,
      content || null,
      { nzClass: `system-http_notification warning ${!content && 'only-title'}` },
    );
  }

}
