import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../src/environments/environment';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(public http: HttpClient) {}

  getAll(params) {
    return this.http.get(`${apiUrl}/api/role`, { params });
  }

  get(params) {
    return this.http.get(`${apiUrl}/api/role/${params}`);
  }

  save(params) {
    return this.http.post(`${apiUrl}/api/role`, params.data);
  }

  update(params) {
    return this.http.put(`${apiUrl}/api/role/${params.id}`, params.data);
  }

  delete(params) {
    return this.http.delete(`${apiUrl}/api/role/${params.id}`);
  }
}
