import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../src/environments/environment';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http: HttpClient) {}

  getAll(params) {
    return this.http.get(`${apiUrl}/api/user`, { params });
  }

  get(params) {
    return this.http.get(`${apiUrl}/api/user/${params}`);
  }

  save(params) {
    return this.http.post(`${apiUrl}/api/user`, params.data);
  }

  update(params) {
    return this.http.put(`${apiUrl}/api/user/${params.id}`, params.data);
  }

  delete(params) {
    return this.http.delete(`${apiUrl}/api/user/${params.id}`);
  }

}
