import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';

import { AuthActionTypes, authActions } from '../actions/auth';
import { AuthService } from '../services/auth';

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private authService: AuthService
  ) {}

  signin$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActionTypes.AuthSignin),
      switchMap((params) =>
        this.authService.signin(params).pipe(
          mergeMap((data: any) => [
            authActions.setCurrentUser({ data }),
            authActions.successRequest(),
          ]),
          catchError(data =>
            of(
              authActions.errorRequest({ data })
            )
          )
        )
      )
    );
  });

  verifyUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActionTypes.VerifyUser),
      switchMap((params) =>
        this.authService.verifyUser(params).pipe(
          mergeMap((data: any) => [
            authActions.verified(),
            authActions.successRequest(),
          ]),
          catchError(data =>
            of(
              authActions.errorRequest({ data })
            )
          )
        )
      )
    );
  });

  refreshToken$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActionTypes.RefreshToken),
      switchMap((params) =>
        this.authService.refreshToken(params).pipe(
          mergeMap((data: any) => [
            authActions.refreshed(),
            authActions.successRequest(),
          ]),
          catchError(data =>
            of(
              authActions.errorRequest({ data })
            )
          )
        )
      )
    );
  });

}
