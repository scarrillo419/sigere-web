import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';

import { UserActionTypes, userActions } from '../actions/user';
import { UserService } from '../services/user';
import { RoleService } from '../services/role';

@Injectable()
export class UserEffects {

  constructor(
    private actions$: Actions,
    private userService: UserService,
    private roleService: RoleService,
  ) {}

  getAllUsers$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActionTypes.GetAll),
      switchMap((params) =>
        this.userService.getAll(params).pipe(
          mergeMap((data: any) => [
            userActions.setAllUsers({ data }),
            userActions.successRequest(),
          ]),
          catchError(data =>
            of(
              userActions.errorRequest()
            )
          )
        )
      )
    );
  });

  getUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActionTypes.Get),
      switchMap((params) =>
        this.userService.get(params).pipe(
          mergeMap((data: any) => [
            userActions.setUser({ data }),
            userActions.successRequest(),
          ]),
          catchError(data =>
            of(
              userActions.errorRequest()
            )
          )
        )
      )
    );
  });

  saveUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActionTypes.Save),
      switchMap((params) =>
        this.userService.save(params).pipe(
          mergeMap(() => [
            userActions.successRequest(),
            userActions.cancelNewUser(),
            userActions.getAllUsers(),
          ]),
          catchError(data =>
            of(
              userActions.errorRequest()
            )
          )
        )
      )
    );
  });

  updateUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActionTypes.Update),
      switchMap((params) =>
        this.userService.update(params).pipe(
          mergeMap(() => [
            userActions.successRequest(),
            userActions.cancelNewUser(),
            userActions.getAllUsers(),
          ]),
          catchError(data =>
            of(
              userActions.errorRequest()
            )
          )
        )
      )
    );
  });

  deleteUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActionTypes.Delete),
      switchMap((params) =>
        this.userService.delete(params).pipe(
          mergeMap(() => [
            userActions.successRequest(),
            userActions.getAllUsers(),
          ]),
          catchError(data =>
            of(
              userActions.errorRequest()
            )
          )
        )
      )
    );
  });

  getRoles$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActionTypes.GetRoles),
      switchMap((params) =>
        this.roleService.getAll(params).pipe(
          mergeMap((data: any) => [
            userActions.successRequest(),
            userActions.setRoles({ data }),
          ]),
          catchError(data =>
            of(
              userActions.errorRequest()
            )
          )
        )
      )
    );
  });

}
