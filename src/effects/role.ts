import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';

import { RoleActionTypes, roleActions } from '../actions/role';
import { RoleService } from '../services/role';

@Injectable()
export class RoleEffects {

  constructor(
    private actions$: Actions,
    private roleService: RoleService
  ) {}

  getAllRoles$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleActionTypes.GetAll),
      switchMap((params) =>
        this.roleService.getAll(params).pipe(
          mergeMap((data: any) => [
            roleActions.setAllRoles({ data }),
            roleActions.successRequest(),
          ]),
          catchError(data =>
            of(
              roleActions.errorRequest()
            )
          )
        )
      )
    );
  });

  getRole$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleActionTypes.Get),
      switchMap((params) =>
        this.roleService.get(params).pipe(
          mergeMap((data: any) => [
            roleActions.setRole({ data }),
            roleActions.successRequest(),
          ]),
          catchError(data =>
            of(
              roleActions.errorRequest()
            )
          )
        )
      )
    );
  });

  saveRole$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleActionTypes.Save),
      switchMap((params) =>
        this.roleService.save(params).pipe(
          mergeMap(() => [
            roleActions.successRequest(),
            roleActions.cancelNewRole(),
            roleActions.getAllRoles(),
          ]),
          catchError(data =>
            of(
              roleActions.errorRequest()
            )
          )
        )
      )
    );
  });

  updateRole$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleActionTypes.Update),
      switchMap((params) =>
        this.roleService.update(params).pipe(
          mergeMap(() => [
            roleActions.successRequest(),
            roleActions.cancelNewRole(),
            roleActions.getAllRoles(),
          ]),
          catchError(data =>
            of(
              roleActions.errorRequest()
            )
          )
        )
      )
    );
  });

  deleteRole$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleActionTypes.Delete),
      switchMap((params) =>
        this.roleService.delete(params).pipe(
          mergeMap(() => [
            roleActions.successRequest(),
            roleActions.getAllRoles(),
          ]),
          catchError(data =>
            of(
              roleActions.errorRequest()
            )
          )
        )
      )
    );
  });

}
