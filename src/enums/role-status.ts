export enum RoleStatusEnum {
  active = 'active',
  inactive = 'inactive',
  remove = 'remove',
}

export const RoleStatusEnumAsArray = Object.keys(RoleStatusEnum);

export const RoleStatusEnumAsKeyValue = Object.keys(RoleStatusEnum).map((key: any) => ({
  value: key,
  text: RoleStatusEnum[key],
}));

