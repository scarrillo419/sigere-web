export enum RolesEnum {
  owner = 'owner',
  client = 'client',
}
export const RolesEnumAsArray = Object.keys(RolesEnum) as RolesEnum[];

export const RolesEnumKeyValue = Object.keys(RolesEnum).map((key: any) => ({
  value: key,
  text: RolesEnum[key],
}));
