import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { authActions } from '../../../actions/auth';

@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.less']
})
export class AppLayoutComponent implements OnInit {

  name$: Observable<boolean>;

  constructor(
    private store: Store<any>,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.name$ = this.store.select(store => store.auth.user ? store.auth.user.name : null);
  }

  signout() {
    this.store.dispatch(authActions.signout());
  }

  setLanguage(lang: any) {
    this.translate.setDefaultLang(lang);
  }

}
