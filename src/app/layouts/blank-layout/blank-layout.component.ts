import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-blank-layout',
  templateUrl: './blank-layout.component.html',
  styleUrls: ['./blank-layout.component.less']
})
export class BlankLayoutComponent implements OnInit {

  private selectedValue = 'en';

  constructor(private translate: TranslateService) { }

  ngOnInit() {
  }

  setLanguage(lang: any) {
    this.translate.setDefaultLang(lang);
  }

}
