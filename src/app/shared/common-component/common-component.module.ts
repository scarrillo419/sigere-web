import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { AngularCropperjsModule } from 'angular-cropperjs';

import { NotificationComponent } from './notification/notification.component';
import { ImageCropperComponent } from './image-cropper/image-cropper.component';

@NgModule({
  declarations: [
    NotificationComponent,
    ImageCropperComponent,
  ],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    AngularCropperjsModule,
  ],
  exports: [
    ImageCropperComponent,
  ]
})
export class CommonComponentModule { }
