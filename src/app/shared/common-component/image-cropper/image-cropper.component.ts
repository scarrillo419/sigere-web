import { Component, OnInit, ViewChild, forwardRef, Output, EventEmitter, Input } from '@angular/core';
import { CropperComponent } from 'angular-cropperjs';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ImageCropperComponent),
      multi: true
    }
  ],
})
export class ImageCropperComponent implements OnInit, ControlValueAccessor {
  @ViewChild('angularCropper', { static: false }) private angularCropper: CropperComponent;
  @Input() ambit = 'users';
  @Input() value = null;
  @Output() fileCrop = new EventEmitter<any>();

  imageUrl = '';
  options = {
    dragMode: 'move',
    aspectRatio: 1,
    minContainerWidth: 200,
    minContainerHeight: 200,
    zoomOnWheel: true,
    movable: true,
    zoomable: true,
    cropBoxResizable: false,
    cropBoxMovable: false,
  };
  imageDefault = '../../../assets/img/default/image-default.png';
  imageCropped = this.imageDefault;
  isCropping = false;
  cropData = null;
  file = null;

  userFilePath = `${environment.apiUrl}/api/public/${this.ambit}`;

  constructor() { }

  ngOnInit() {
  }

  fileChange(e: any) {
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();

      reader.onload = (_e: any) => {
        this.imageUrl = _e.target.result;
        this.isCropping = true;
        this.file = e.target.files[0];
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  }

  cancelCrop() {
    this.angularCropper.cropper.destroy();
    this.file = null;
    this.isCropping = false;
  }

  crop() {
    this.imageCropped = this.angularCropper.cropper.getCroppedCanvas().toDataURL();
    this.cropData = this.angularCropper.cropper.getData();
    this.isCropping = false;
    this.fileCrop.emit(this.file);
    this.propagateChange(this.cropData);
  }

  removeImage() {
    this.imageCropped = this.imageDefault;
    this.file = null;
    this.value = null;
    this.cropData = null;
    this.fileCrop.emit(this.file);
    this.propagateChange(this.cropData);
  }

  propagateChange = (_: any) => {};

  writeValue(value: any) {
    if (this.value) {
      this.imageCropped = `${this.userFilePath}/${this.value}`;
    } else {
      this.imageCropped = this.imageDefault;
    }
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched() {}
}
