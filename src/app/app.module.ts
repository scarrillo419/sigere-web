import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, en_US, NzConfig, NZ_CONFIG } from 'ng-zorro-antd';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import en from '@angular/common/locales/en';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IconsProviderModule } from './icons-provider.module';

import { BlankLayoutComponent } from './layouts/blank-layout/blank-layout.component';
import { AppLayoutComponent } from './layouts/app-layout/app-layout.component';

import { reducers } from '../reducers/index';

import { AuthEffects } from '../effects/auth';
import { UserEffects } from '../effects/user';
import { RoleEffects } from '../effects/role';
import { CommonComponentModule } from './shared/common-component/common-component.module';

import { HttpRequestInterceptor } from '../interceptors/http.interceptor';

import { storageMetaReducer } from '../meta-reducers/index';
import { CheckSessionGuard } from '../guards/check-session';

registerLocaleData(en);

const ngZorroConfig: NzConfig = {
  notification: {
    nzPlacement: 'bottomRight',
    nzPauseOnHover: true,
    // nzDuration: 0,
  },
};

@NgModule({
  declarations: [
    AppComponent,
    BlankLayoutComponent,
    AppLayoutComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IconsProviderModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(reducers, {
      metaReducers: [storageMetaReducer]
    }),
    // EffectsModule.forRoot([AuthEffects]),
    EffectsModule.forRoot([
      AuthEffects,
      UserEffects,
      RoleEffects,
    ]),
    CommonComponentModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    {
      provide: NZ_CONFIG,
      useValue: ngZorroConfig,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpRequestInterceptor,
      multi: true
    },
    {
      provide: NZ_I18N,
      useValue: en_US,
    },
    CheckSessionGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
