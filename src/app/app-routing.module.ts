import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlankLayoutComponent } from './layouts/blank-layout/blank-layout.component';
import { AppLayoutComponent } from './layouts/app-layout/app-layout.component';
import { CheckSessionGuard } from '../guards/check-session';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/authentication/signin',
  },
  {
    path: 'authentication',
    component: BlankLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
      }
    ],
  },
  {
    path: 'app',
    component: AppLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule),
        canActivate: [CheckSessionGuard],
      }
    ],
  },
  {path: '**', redirectTo: '/authentication/signin'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
