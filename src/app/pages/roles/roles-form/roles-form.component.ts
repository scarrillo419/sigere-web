import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { roleActions } from '../../../../actions/role';
import { Role } from '../../../../models/role';
import { SharedService } from '../../../../services/shared';
import { prepareData } from '../../../../utils';

@Component({
  selector: 'app-roles-form',
  templateUrl: './roles-form.component.html',
  styleUrls: ['./roles-form.component.less']
})
export class RolesFormComponent implements OnInit {
  @Input() show = false;
  @Input() loading = false;
  @Input() role = new Role({});

  form: FormGroup;

  constructor(
    private store: Store<any>,
    private fb: FormBuilder,
    private sharedService: SharedService,
  ) { }

  ngOnInit() {
    this.initForm(this.role);
  }

  initForm(data?: any) {
    this.form = this.fb.group({
      name: [data ? data.name : null, [Validators.required]],
    });
  }

  afterOpen() {
    if (this.role._id) {
      this.initForm(this.role);
    }
  }

  afterClose() {
    this.initForm();
  }

  handleCancel() {
    this.store.dispatch(roleActions.cancelNewRole());
  }

  handleOk() {
    for (const i in this.form.controls) {
      if (i) {
        this.form.controls[i].markAsDirty();
        this.form.controls[i].updateValueAndValidity();
      }
    }

    if (this.form.valid) {
      const { saveRole, updateRole } = roleActions;
      const fields = this.form.getRawValue();

      if (this.role._id) {
        this.store.dispatch(updateRole({ id: this.role._id, data: fields }));
      } else {
        this.store.dispatch(saveRole({ data: fields }));
      }
    } else {
      this.sharedService.error('Completa los campos requeridos');
    }
  }
}
