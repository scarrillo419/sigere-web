import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { roleActions } from '../../../../actions/role';
import { PaginateResult } from '../../../../interfaces/PaginateResult';
import { Role } from '../../../../models/role';

@Component({
  selector: 'app-roles-table',
  templateUrl: './roles-table.component.html',
  styleUrls: ['./roles-table.component.less']
})
export class RolesTableComponent implements OnInit, OnDestroy {
  unsubscribe$ = new Subject<void>();
  roleStore$: Observable<any>;
  loading = false;
  show = false;
  role = new Role({});
  roles: PaginateResult<Role>;

  constructor(private store: Store<any>) { }

  ngOnInit() {
    this.store.select(store => store.role)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(role => {
        this.show = role.show;
        this.loading = role.loading;
        this.role = role.role;
        this.roles = role.listRoles;
      });
    this.getRoles();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getRoles() {
    this.store.dispatch(roleActions.getAllRoles());
  }

  newRole() {
    this.store.dispatch(roleActions.newRole());
  }

  editRole(role: Role) {
    this.store.dispatch(roleActions.editRole({ data: role }));
  }

  deleteRole(id) {
    this.store.dispatch(roleActions.deleteRole({ id }));
  }
}
