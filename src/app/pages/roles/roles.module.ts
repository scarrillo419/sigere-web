import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RolesRoutingModule } from './roles-routing.module';
import { RolesFormComponent } from './roles-form/roles-form.component';
import { RolesTableComponent } from './roles-table/roles-table.component';


@NgModule({
  declarations: [
    RolesFormComponent,
    RolesTableComponent,
  ],
  imports: [
    CommonModule,
    RolesRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class RolesModule { }
