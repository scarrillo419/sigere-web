import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RolesTableComponent } from '../roles/roles-table/roles-table.component';

const routes: Routes = [
  {
    path: '',
    component: RolesTableComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesRoutingModule { }
