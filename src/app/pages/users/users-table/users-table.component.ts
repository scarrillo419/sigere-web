import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { userActions } from '../../../../actions/user';
import { PaginateResult } from '../../../../interfaces/PaginateResult';
import { User } from '../../../../models/user';
import { Role } from '../../../../models/role';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.less']
})
export class UsersTableComponent implements OnInit, OnDestroy {
  unsubscribe$ = new Subject<void>();
  userStore$: Observable<any>;
  loading = false;
  show = false;
  user = new User({});
  users: PaginateResult<User>;
  loadingRoles = false;
  roles: PaginateResult<Role>;
  open = false;

  imageDefault = '../../../assets/img/default/image-default.png';
  userFilePath = `${environment.apiUrl}/api/public/users`;

  constructor(private store: Store<any>) { }

  ngOnInit() {
    this.store.select(store => store.user)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => {
        this.show = user.show;
        this.loading = user.loading;
        this.user = user.user;
        this.users = user.listUsers;
        this.loadingRoles = user.loadingRoles;
        this.roles = user.listRoles;
        this.open = user.open;
      });
    this.getUsers();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getUsers() {
    this.store.dispatch(userActions.getAllUsers());
  }

  newUser() {
    this.store.dispatch(userActions.newUser());
  }

  editUser(user: User) {
    this.store.dispatch(userActions.editUser({ data: user }));
  }

  deleteUser(id) {
    this.store.dispatch(userActions.deleteUser({ id }));
  }

  getRoles() {
    this.store.dispatch(userActions.getRoles());
  }
}
