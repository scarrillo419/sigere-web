import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { userActions } from '../../../../actions/user';
import { User } from '../../../../models/user';
import { Role } from '../../../../models/role';
import { SharedService } from '../../../../services/shared';
import { prepareData } from '../../../../utils';
import { RolesEnumKeyValue } from '../../../../enums/roles';
import { PaginateResult } from '../../../../interfaces/PaginateResult';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.less']
})
export class UsersFormComponent implements OnInit {
  @Input() show = false;
  @Input() loading = false;
  @Input() loadingRoles = false;
  @Input() user = new User({});
  @Input() roles: PaginateResult<Role>;
  @Input() open = false;
  @Output() getRoles = new EventEmitter();

  form: FormGroup;
  // roles = RolesEnumKeyValue;
  // open = false;

  constructor(
    private store: Store<any>,
    private fb: FormBuilder,
    private sharedService: SharedService,
  ) { }

  ngOnInit() {
    this.initForm(this.user);
  }

  initForm(data?: any) {
    this.form = this.fb.group({
      name: [data ? data.name : null, [Validators.required]],
      lastName: [data ? data.lastName : null, [Validators.required]],
      email: [data ? data.email : null, [Validators.required]],
      roles: [data ? data.roles : null, [Validators.required]],
      file: [null],
      crooperData: [null],
    });
  }

  changeFileCrop(e: any) {
    this.form.get('file').setValue(e);
  }

  afterOpen() {
    if (this.user._id) {
      this.initForm(this.user);
    }
  }

  afterClose() {
    this.initForm();
  }

  openSelect(e: any) {
    if (e) {
      this.getRoles.emit();
      return true;
    }
  }

  handleCancel() {
    this.store.dispatch(userActions.cancelNewUser());
  }

  handleOk() {
    for (const i in this.form.controls) {
      if (i) {
        this.form.controls[i].markAsDirty();
        this.form.controls[i].updateValueAndValidity();
      }
    }

    if (this.form.valid) {
      const { saveUser, updateUser } = userActions;
      const fields = this.form.getRawValue();
      const formData = prepareData(fields);

      if (this.user._id) {
        this.store.dispatch(updateUser({ id: this.user._id, data: formData }));
      } else {
        this.store.dispatch(saveUser({ data: formData }));
      }
    } else {
      this.sharedService.error('Completa los campos requeridos');
    }
  }
}
