import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { AngularCropperjsModule } from 'angular-cropperjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UsersRoutingModule } from './users-routing.module';
import { UsersTableComponent } from './users-table/users-table.component';
import { UsersFormComponent } from './users-form/users-form.component';
import { CommonComponentModule } from '../../shared/common-component/common-component.module';

@NgModule({
  declarations: [
    UsersTableComponent,
    UsersFormComponent,
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    NgZorroAntdModule,
    AngularCropperjsModule,
    CommonComponentModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class UsersModule { }
