import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { SigninComponent } from './signin/signin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { VerifyUserComponent } from './verify-user/verify-user.component';


@NgModule({
  declarations: [
    SigninComponent,
    VerifyUserComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    SigninComponent,
  ],
})
export class AuthModule { }
