import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { IsAuthenticatedGuard } from '../../guards/is-authenticated';
import { VerifyUserComponent } from './verify-user/verify-user.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'signin',
  },
  {
    path: 'signin',
    component: SigninComponent,
    canActivate: [IsAuthenticatedGuard]
  },
  {
    path: 'verify-user/:token',
    component: VerifyUserComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
