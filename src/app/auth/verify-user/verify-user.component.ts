import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { authActions } from '../../../actions/auth';

@Component({
  selector: 'app-verify-user',
  templateUrl: './verify-user.component.html',
  styleUrls: ['./verify-user.component.less']
})
export class VerifyUserComponent implements OnInit, OnDestroy {
  unsubscribe$ = new Subject<void>();
  loading = false;
  verified = false;
  refreshed = false;
  error = null;
  token = null;

  constructor(
    private store: Store<any>,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token');

    this.store.select(store => store.auth)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(auth => {
        console.log('auth: ', auth);
        this.loading = auth.loading;
        this.refreshed = auth.refreshed;
        this.verified = auth.verified;
        this.error = auth.error;
      });
    this.store.dispatch(authActions.verifyUser({ data: this.token }));
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  refreshToken() {
    this.store.dispatch(authActions.refreshToken({ data: this.token }));
  }
}
