export class User {
  _id: string;
  name: string;
  lastName: string;
  photo: string;
  email: string;
  status: string;
  roles: string[];
  lastLogin: Date;
  language: string;

  constructor(fields: any) {
    this._id = fields._id;
    this.name = fields.name;
    this.lastName = fields.lastName;
    this.photo = fields.photo;
    this.email = fields.email;
    this.status = fields.status;
    this.roles = fields.roles || [];
    this.lastLogin = fields.lastLogin;
    this.language = fields.language;
  }
}
