import { RoleStatusEnum } from '../enums/role-status';

export class Role {
  public _id: string;
  public name: string;
  public modules: string[];
  readonly status: RoleStatusEnum;

  constructor(info: any) {
      this._id = info._id;
      this.name = info.name;
      this.modules = info.modules;
      this.status = info.status;
  }
}
