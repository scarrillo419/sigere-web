import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { roleActions } from '../actions/role';
import { Role } from '../models/role';
import { PaginateResult } from '../interfaces/PaginateResult';

export const ROLE_FEATURE_KEY = 'role';

export interface State extends EntityState<any> {
  listRoles: PaginateResult<Role>;
  role: Role;
  show: boolean;
  loading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<any> = createEntityAdapter<any>({
  selectId: item => item._id
});

export interface RolePartialState {
  readonly [ROLE_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  listRoles: {
    docs: [],
    total: 0,
    limit: 10,
    page: 1,
    pages: 0,
    offset: 0,
  },
  role: new Role({}),
  show: false,
  loading: false,
  error: null
});

const _reducer = createReducer(
  initialState,
  on(roleActions.getAllRoles, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(roleActions.getRole, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(roleActions.setAllRoles, (state, { data }) => {
    return {
      ...state,
      listRoles: data,
    };
  }),
  on(roleActions.setRole, (state, { data }) => {
    return {
      ...state,
      role: data,
    };
  }),
  on(roleActions.newRole, (state) => {
    return {
      ...state,
      role: new Role({}),
      show: true,
    };
  }),
  on(roleActions.cancelNewRole, (state) => {
    return {
      ...state,
      role: new Role({}),
      show: false,
    };
  }),
  on(roleActions.editRole, (state, { data }) => {
    return {
      ...state,
      role: new Role(data),
      show: true,
    };
  }),
  on(roleActions.updateRole, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(roleActions.errorRequest, (state) => {
    return {
      ...state,
      loading: false,
    };
  }),
  on(roleActions.successRequest, (state) => {
    return {
      ...state,
      loading: false,
    };
  }),
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
