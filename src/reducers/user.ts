import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { userActions } from '../actions/user';
import { User } from '../models/user';
import { PaginateResult } from '../interfaces/PaginateResult';

export const USER_FEATURE_KEY = 'user';

export interface State extends EntityState<any> {
  listUsers: PaginateResult<User>;
  user: User;
  show: boolean;
  loading: boolean;
  loadingRoles: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<any> = createEntityAdapter<any>({
  selectId: item => item._id
});

export interface UserPartialState {
  readonly [USER_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  listUsers: {
    docs: [],
    total: 0,
    limit: 10,
    page: 1,
    pages: 0,
    offset: 0,
  },
  user: new User({}),
  show: false,
  loading: false,
  loadingRoles: false,
  listRoles: {
    docs: [],
    total: 0,
    limit: 10,
    page: 1,
    pages: 0,
    offset: 0,
  },
  open: false,
  error: null
});

const _reducer = createReducer(
  initialState,
  on(userActions.getAllUsers, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(userActions.getUser, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(userActions.setAllUsers, (state, { data }) => {
    return {
      ...state,
      listUsers: data,
    };
  }),
  on(userActions.setUser, (state, { data }) => {
    return {
      ...state,
      user: data,
    };
  }),
  on(userActions.newUser, (state) => {
    return {
      ...state,
      user: new User({}),
      show: true,
    };
  }),
  on(userActions.cancelNewUser, (state) => {
    return {
      ...state,
      user: new User({}),
      show: false,
    };
  }),
  on(userActions.editUser, (state, { data }) => {
    return {
      ...state,
      user: new User(data),
      show: true,
    };
  }),
  on(userActions.updateUser, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(userActions.getRoles, (state) => {
    return {
      ...state,
      open: false,
      loadingRoles: true,
    };
  }),
  on(userActions.setRoles, (state, { data }) => {
    return {
      ...state,
      open: true,
      listRoles: data,
    };
  }),
  on(userActions.errorRequest, (state) => {
    return {
      ...state,
      loading: false,
      loadingRoles: false,
    };
  }),
  on(userActions.successRequest, (state) => {
    return {
      ...state,
      loading: false,
      loadingRoles: false,
    };
  }),
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
