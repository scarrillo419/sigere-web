import * as fromAuth from './auth';
import * as fromUser from './user';
import * as fromRole from './role';

export const reducers = {
  auth: fromAuth.reducer,
  user: fromUser.reducer,
  role: fromRole.reducer,
};
