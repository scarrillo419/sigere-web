import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { isEmpty } from 'lodash';

import { authActions } from '../actions/auth';
import { User } from '../models/user';

export const AUTH_FEATURE_KEY = 'auth';

export interface State extends EntityState<any> {
  readonly user: User;
  readonly settings: any;
  readonly authenticated: boolean;
  readonly verified: boolean;
  readonly refreshed: boolean;
  readonly refresh_token?: string;
  readonly access_token?: string;
  loading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<any> = createEntityAdapter<any>({
  // In this case this would be optional since primary key is id
  selectId: item => item._id
});

export interface AuthPartialState {
  readonly [AUTH_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  // Additional entity state properties
  user: new User({}),
  settings: {},
  authenticated: false,
  verified: false,
  refreshed: false,
  refresh_token: undefined,
  access_token: undefined,
  loading: false,
  error: null
});

const _reducer = createReducer(
  initialState,
  on(authActions.setCurrentUser, (state, { data }) => {
    return {
      ...state,
      user: data.user,
      settings: data.settings,
      authenticated: !isEmpty(data.user),
      refresh_token: data.refresh_token,
      access_token: data.access_token,
    };
  }),
  on(authActions.signin, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(authActions.signout, (state) => {
    return {
      ...state,
      user: null,
      settings: null,
      authenticated: false,
      refresh_token: null,
      access_token: null,
    };
  }),
  on(authActions.verifyUser, (state) => {
    return {
      ...state,
      verified: false,
      refreshed: false,
      loading: true,
      error: null,
    };
  }),
  on(authActions.verified, (state) => {
    return {
      ...state,
      verified: true,
    };
  }),
  on(authActions.refreshToken, (state) => {
    return {
      ...state,
      verified: false,
      refreshed: false,
      loading: true,
      error: null,
    };
  }),
  on(authActions.refreshed, (state) => {
    return {
      ...state,
      refreshed: true,
    };
  }),
  on(authActions.errorRequest, (state, { data }) => {
    return {
      ...state,
      loading: false,
      error: data,
    };
  }),
  on(authActions.successRequest, (state) => {
    return {
      ...state,
      loading: false,
    };
  }),
);

export function reducer(state: State | undefined, action: Action) {
  return _reducer(state, action);
}
